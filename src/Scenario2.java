import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Scenario2 {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Kalyani\\Downloads\\chromedriver_win32 (3)\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		String baseUrl = "https://www.growthengineering.co.uk/";
		driver.manage().window().maximize();
		String expectedTitle = "Growth Engineering - Online Learning Engagement Experts";
		String actualTitle = "";
		driver.get(baseUrl);
		actualTitle = driver.getTitle();
		if (actualTitle.contentEquals(expectedTitle)) {
			System.out.println("Welcome to Home Page");
		} else {
			System.out.println("Test case Failed");
		}
		driver.findElement(By.xpath("//span[text()='GET IN TOUCH']")).click();
		String expectedText = "CONTACT US TODAY!";
		String actualText = "";
		actualText = driver.findElement(By.xpath("//*[text()='CONTACT US TODAY!']")).getText();
		if (actualText.contentEquals(expectedText)) {
			System.out.println("Get In Touch Passed!");
		} else {
			System.out.println("Get In Touch Failed");
		}
	}
}